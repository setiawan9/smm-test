<?php

namespace Modules\Barang\Entities;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
		protected $fillable = [];
		protected $table = 'ref_barang';
}
