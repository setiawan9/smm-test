<?php

namespace Modules\Peminta\Entities;

use Illuminate\Database\Eloquent\Model;

class Peminta extends Model
{
		protected $fillable = [];
		protected $table = 'ref_peminta';
		
		public function requests(){
			return $this->hasMany('Modules\PermintaanBarang\Entities\PermintaanBarang', 'nik_peminta', 'nik');
		}
}
