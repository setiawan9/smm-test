<?php

namespace Modules\Peminta\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Peminta\Entities\Peminta;

class PemintaController extends Controller
{
	/**
	 * Display a listing of the resource.
	 * @return Response
	 */
	public function index()
	{
		$peminta = Peminta::all();
		return response()->json($peminta);
	}

}
