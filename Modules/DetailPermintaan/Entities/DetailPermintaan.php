<?php

namespace Modules\DetailPermintaan\Entities;

use Illuminate\Database\Eloquent\Model;

class DetailPermintaan extends Model
{
	protected $fillable = ['permintaan_id', 'id_barang', 'qty', 'ket'];
	protected $table = 'trc_detail_permintaan';

	public function barang()
	{
		return $this->hasOne('Modules\Barang\Entities\Barang', 'id', 'id_barang');
	}
}
