const routes = [
	{
		path: '/',
		props: true,
		component: require('./components/Index.vue').default
	},
	{
		path: '/permintaan/create',
		props: true,
		name: 'permintaan.create',
		component: require('./components/Create.vue').default
	}
]

export default routes