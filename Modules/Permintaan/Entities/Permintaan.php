<?php

namespace Modules\Permintaan\Entities;

use Illuminate\Database\Eloquent\Model;

class Permintaan extends Model
{
	protected $fillable = ['nik_peminta', 'tgl_permintaan'];
	protected $table = 'trc_permintaan';

	public function permintaan_details()
	{
		return $this->hasMany('Modules\DetailPermintaan\Entities\DetailPermintaan', 'permintaan_id');
	}

	public function peminta(){
		return $this->hasOne('Modules\Peminta\Entities\Peminta', 'nik', 'nik_peminta');
	}
}
