<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/permintaan', function (Request $request) {
//     return $request->user();
// });

Route::get('/permintaan', 'PermintaanController@index');
Route::get('/permintaan/detail/{id}', 'PermintaanController@show');
Route::get('/permintaan/delete/{id}', 'PermintaanController@delete');
Route::post('/permintaan/store', 'PermintaanController@store');

