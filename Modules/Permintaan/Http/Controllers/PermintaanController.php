<?php

namespace Modules\Permintaan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Permintaan\Entities\Permintaan;
use Modules\DetailPermintaan\Entities\DetailPermintaan;
use Modules\Barang\Entities\Barang;

class PermintaanController extends Controller
{
	/**
	 * Display a listing of the resource.
	 * @return Response
	 */
	public function index()
	{
		$data = Permintaan::with('permintaan_details')
											->with('peminta')
											->get();

		return response()->json($data);
	}

	public function show($id)
	{
		$data = Permintaan::where('id', $id)
											->with('permintaan_details.barang')
											->with('peminta')
											->first();

		return response()->json($data);
	}

	public function store(Request $request)
	{
		$peminta = $request->peminta;
		$barang = $request->daftarBarang;
		$tgl_permintaan = $request->tgl_permintaan;



		$id_permintaan = Permintaan::insertGetId([
			'nik_peminta' => $peminta['nik'],
			'tgl_permintaan' => $tgl_permintaan,
		]);


		
		foreach ($barang as $brg) {
			$cekStok = Barang::where('id', $brg['id'])->first();

			if($cekStok->stok > 0 && $cekStok->stok > $brg['kuantiti']){
				$cekStok->stok -= $brg['kuantiti'];
				$cekStok->save();
			}
			// echo $id_permintaan;
			// die;
			DetailPermintaan::create([
				'permintaan_id' => $id_permintaan,
				'id_barang' => $brg['id'],
				'qty' => $brg['kuantiti'],
				'ket' => $brg["ket"],
				'status' => $brg['status']
			]);
		}
	}

	public function delete($id){
		$permintaan = Permintaan::findOrFail($id);
		$permintaan->permintaan_details()->delete();
		$permintaan->delete();
	}
}
