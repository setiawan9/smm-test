<?php

use Illuminate\Database\Seeder;

class TrcPermintaanTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('trc_permintaan')->delete();
        
        \DB::table('trc_permintaan')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nik_peminta' => '001.005.077',
                'tgl_permintaan' => '2020-07-17 00:00:00',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nik_peminta' => '002.044.079',
                'tgl_permintaan' => '2020-07-24 00:00:00',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}