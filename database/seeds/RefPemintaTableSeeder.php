<?php

use Illuminate\Database\Seeder;

class RefPemintaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('ref_peminta')->delete();
        
        \DB::table('ref_peminta')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nik' => '001.005.077',
                'nama' => 'Dodi Setiawan',
                'departemen' => 'Callendar',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nik' => '002.044.079',
                'nama' => 'Aam Suherman',
                'departemen' => 'Callendar',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}