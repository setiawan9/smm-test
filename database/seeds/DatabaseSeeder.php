<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(RefBarangTableSeeder::class);
        $this->call(RefPemintaTableSeeder::class);
        $this->call(TrcDetailPermintaanTableSeeder::class);
        $this->call(TrcPermintaanTableSeeder::class);
    }
}
