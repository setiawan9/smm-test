<?php

use Illuminate\Database\Seeder;

class TrcDetailPermintaanTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('trc_detail_permintaan')->delete();
        
        \DB::table('trc_detail_permintaan')->insert(array (
            0 => 
            array (
                'id' => 1,
                'permintaan_id' => 1,
                'id_barang' => 3,
                'qty' => 5,
                'ket' => 'ok',
                'created_at' => '2020-07-13 21:44:23',
                'updated_at' => '2020-07-13 21:44:23',
            ),
            1 => 
            array (
                'id' => 2,
                'permintaan_id' => 2,
                'id_barang' => 3,
                'qty' => 12,
                'ket' => '11',
                'created_at' => '2020-07-13 21:49:46',
                'updated_at' => '2020-07-13 21:49:46',
            ),
        ));
        
        
    }
}