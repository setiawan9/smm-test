<?php

use Illuminate\Database\Seeder;

class RefBarangTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('ref_barang')->delete();
        
        \DB::table('ref_barang')->insert(array (
            0 => 
            array (
                'id' => 1,
                'kode_barang' => 'ATK0001',
                'nama_barang' => 'Amplop A Coklat Jaya',
                'lokasi' => 'L1-R1A',
                'stok' => 30,
                'satuan' => 'Pak',
                'status' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'kode_barang' => 'ATK0002',
                'nama_barang' => 'Amplop B Coklat Jaya',
                'lokasi' => 'L1-R1A',
                'stok' => 15,
                'satuan' => 'Pak',
                'status' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-07-13 22:44:44',
            ),
            2 => 
            array (
                'id' => 3,
                'kode_barang' => 'ATK0003',
                'nama_barang' => 'Amplop C Coklat Jaya',
                'lokasi' => 'L1-R1A',
                'stok' => 15,
                'satuan' => 'Pak',
                'status' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}