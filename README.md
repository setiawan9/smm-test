## Laravel v 7.x

Include file .env

Pembuatan menggunakan laravel modular, seeder dan migrasi sudah di setting tinggal di jalankan saja
frontend vue.js (tanpa menggunakan vuex sebagai state management karena ruang lingkup nya hanya sedikit)


Installasi :,
```sh
composer update
npm install
php artisan migrate
php artisan db:seed
```



### Referensi Plugin/tools

- HMVC Laravel Modules ([nwidart](https://nwidart.com/laravel-modules/v4/advanced-tools/artisan-commands))

- Seeder ([iseed](https://github.com/orangehill/iseed))

- API ([axios](https://github.com/axios/axios))

- Pretty Routes ([axios](https://github.com/garygreen/pretty-routes))

